# CICD_Hexagone_Val

# Exam :
Partie 1 : Préparation du Dataset :+1:

Partie 2 : Entraînement d'un Modèle de Machine Learning :+1:

Partie 3 : Conteneurisation avec Docker :+1:

Partie 4 : Automatisation CI/CD avec GitLab :+1:

Partie 5 : Gestion des Données avec DVC :-1:


# Réponse Question : 
1. DevOps est une approche qui combine le développement de logiciels (Dev) et les opérations informatiques (Ops) pour accélérer le cycle de vie du développement de logiciels et fournir une livraison continue de logiciels de haute qualité. Il favorise la collaboration entre les équipes de développement et d'opérations, améliorant ainsi l'efficacité et la productivité.

2. MLOps, ou DevOps pour l'apprentissage automatique, est une pratique d'ingénierie qui vise à unifier l'apprentissage automatique (ML) le développement de systèmes (Dev) et l'exploitation de systèmes d'apprentissage automatique (Ops). MLOps diffère de DevOps en ce sens qu'il gère le cycle de vie unique des modèles de machine learning qui comprend des étapes comme la préparation des données, l'entraînement et l'évaluation du modèle, et le déploiement du modèle.

3. C.A.L.M.S est un acronyme pour Culture, Automation, Lean, Measurement, and Sharing. Il s'agit d'un cadre utilisé pour intégrer les principes DevOps dans une organisation.

4. Six outils couramment utilisés dans la pratique du DevOps et MLOps sont :

Jenkins : un serveur d'automatisation pour l'intégration continue et la livraison continue.
Docker : une plateforme pour développer, expédier, et exécuter des applications dans des conteneurs.
Kubernetes : un système pour automatiser le déploiement, la mise à l'échelle et la gestion des applications conteneurisées.
Git : un système de contrôle de version distribué.
Ansible : un outil pour l'automatisation des tâches informatiques.
MLflow : une plateforme pour gérer le cycle de vie de l'apprentissage automatique, y compris l'expérimentation, la reproduction et le déploiement.

5. L'intégration continue est une pratique de développement de logiciels où les développeurs fusionnent fréquemment leurs modifications de code dans une branche principale. Les modifications sont alors validées par des tests automatisés pour détecter les erreurs le plus tôt possible.

6. La livraison continue est une approche où les logiciels sont développés en cycles courts pour garantir que le logiciel peut être déployé en production à tout moment. Dans MLOps, cela signifie que les modèles de machine learning sont constamment mis à jour et prêts à être déployés.

7. Le déploiement continu est une étape supplémentaire après la livraison continue où chaque modification de code passe par le pipeline d'intégration continue et est automatiquement déployée en production. La différence avec la livraison continue est que la décision de déployer en production est automatisée, tandis qu'avec la livraison continue, la décision est manuelle.

8. Le versionnage sémantique est une convention pour attribuer des versions à votre logiciel afin de communiquer sur les changements qui sont introduits dans chaque nouvelle version. Il est important pour la gestion des versions de logiciels car il aide à suivre les changements et à gérer les dépendances.

9. Quatre fournisseurs de services cloud sont :

Amazon Web Services (AWS) : offre une large gamme de services et est connu pour sa flexibilité et sa scalabilité.
Google Cloud Platform (GCP) : connu pour ses capacités d'analyse de données et de machine learning.
Microsoft Azure : bien intégré avec d'autres produits Microsoft.
IBM Cloud : connu pour ses services d'IA et de blockchain.

10. IaaS (Infrastructure as a Service) fournit aux utilisateurs des ressources informatiques sur Internet. Par exemple, AWS EC2 fournit des capacités de calcul scalables.

11. PaaS (Platform as a Service) fournit une plateforme permettant aux clients de développer, exécuter et gérer des applications sans la complexité de la construction et de la maintenance de l'infrastructure associée. Par exemple, Google App Engine.

12. SaaS (Software as a Service) est un modèle de distribution de logiciels où un fournisseur de services cloud héberge des applications et les rend disponibles aux clients sur Internet. Par exemple, Google Docs.

13. DVC (Data Version Control) est un outil de versionnement pour les projets de machine learning. Il permet de suivre les versions des données et des modèles. Par exemple, la commande dvc add permet de versionner des fichiers ou des dossiers.

14. Un training job est une tâche qui entraîne un modèle de machine learning. Il comprend la préparation des données, la sélection d'un algorithme, la configuration des hyperparamètres et l'entraînement du modèle sur les données.
