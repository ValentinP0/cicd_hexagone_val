# test_app.py
import unittest
import json
from app import app  # import your Flask app

class FlaskAppTests(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_predict_endpoint(self):
        data = {
            "critiques": "This is a test review."
        }
        response = self.app.post('/predict', data=json.dumps(data), content_type='application/json')
        data = json.loads(response.get_data())

        self.assertEqual(response.status_code, 200)
        self.assertIn('positive', data)
        self.assertIn('negative', data)

if __name__ == '__main__':
    unittest.main()