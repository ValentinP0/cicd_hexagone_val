from flask import Flask, request, jsonify
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder
from joblib import load

app = Flask(__name__)
app.debug = True
model = load('model.joblib')  # Charger le modèle entraîné

# Load the fitted vectorizer
vectorizer = load('vectorizer.joblib')

@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json(force=True)
    critiques = [data['critiques']] 
    X = vectorizer.transform(critiques)
    # rateValue = data['labels']
    # le = LabelEncoder()
    # y = le.fit_transform(rateValue)

    prediction = model.predict_proba(X)
    response = {
        'positive': prediction[0][0],
        'negative': prediction[0][1]
    }
    return jsonify(response)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)