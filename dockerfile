# Utiliser une image de base Python 3.8
FROM python:3.8-slim-buster

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers nécessaires dans le conteneur
COPY . /app

RUN apt-get update && apt-get install -y curl

# Installer les dépendances
RUN pip install --no-cache-dir flask scikit-learn pandas joblib

# Exposer le port sur lequel l'application s'exécute
EXPOSE 5000

# Définir la commande pour exécuter l'application
CMD ["python", "app.py"]