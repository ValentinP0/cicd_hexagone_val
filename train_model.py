import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, recall_score
from sklearn.preprocessing import LabelEncoder
from joblib import dump

# Charger le fichier CSV dans un DataFrame
df = pd.read_csv('reviews_unique.csv')

critiques = df['critiques']
rateValue = df['labels']

le = LabelEncoder()
y = le.fit_transform(rateValue)

vectorizer = CountVectorizer()
X = vectorizer.fit_transform(critiques)

# Diviser le dataset en train et validation
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

# Entraîner le modèle de régression logistique
model = LogisticRegression()
model.fit(X_train, y_train)

# Évaluer la performance du modèle
y_pred = model.predict(X_val)
print(f'Précision: {accuracy_score(y_val, y_pred)}')
print(f'Rappel: {recall_score(y_val, y_pred, average=None, zero_division=1)}')

# Sauvegarder le modèle entraîné
dump(model, 'model.joblib')

# Save the fitted vectorizer
dump(vectorizer, 'vectorizer.joblib')