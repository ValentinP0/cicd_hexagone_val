import pandas as pd

# Charger le fichier CSV dans un DataFrame
df = pd.read_csv('reviews_unique.csv')

# Explorer rapidement le dataset
print(df.head())  # Afficher les premières lignes
print(df.info())  # Afficher les informations générales

# Vérifier la présence de doublons
duplicates = df.duplicated()
print(f'Nombre de lignes dupliquées: {duplicates.sum()}')

# Supprimer les doublons si nécessaire
if duplicates.sum() > 0:
    df = df.drop_duplicates()
    print('Doublons supprimés')

print(df.columns)  # Afficher les noms des colonnes

# Save the updated DataFrame to a CSV file
df.to_csv('reviews_unique.csv', index=False)